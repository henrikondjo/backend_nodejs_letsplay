//external imports 

const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });

const connectDb = async () => {
    try {
        await mongoose.connect(process.env.MONGO_DB_URL);
        console.log(`conect to mongodb `);
    } catch (err) {
        console.log(`MongoDb err :${err}`)
    }
}
module.exports = connectDb;