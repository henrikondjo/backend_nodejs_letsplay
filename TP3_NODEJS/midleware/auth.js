const jwtoken = require('jsonwebtoken');

exports.protect = (req, res, next) => {
    const { authorization } = req.headers;

    let token;
    //on recupere notre token dans autorization et le split  a partir d'un espace pour avoir notre token
    if (authorization !== undefined && authorization.startsWith('Bearer')) {
        token = authorization.split(' ')[1];
    }
    //si le token est vide 
    if (!token) {
        return res.send({ 'success': false, 'msg': 'Non autorisé ' });
    }

    try {
        //on decode notre token pour avoir le id du user
        const decoden = jwtoken.verify(token, process.env.JWT_SECRETKEY);
        //garder le id du token dans req pour l'appeler dans nos differentes routes
        req.userid = decoden.id;
    } catch (error) {
        return res.send({ 'success': false, 'msg': 'Access Non autorisé !!' });
    }
    next();
}