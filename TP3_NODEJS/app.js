const express = require('express');

const app = express();
const dotenv = require('dotenv');
//import fichier env variable environnement
dotenv.config({ path: './config/config.env' });
//body parser
app.use(express.json());
//internal imports
const connectDb = require('./config/database')
connectDb();
//notre initial point
app.get('/', (req, res) => {
    res.send('initial point');
})
//appel des differente route et leur logiques
const authRouter = require('./routes/auth.js')
const meRouter = require('./routes/user.js')
const gameRouter = require('./routes/game.js')
app.use('/game', gameRouter);
app.use('/auth', authRouter);
app.use('/users', meRouter);

app.listen(process.env.PORT, () => {
    console.log(`je t'ecoute au port ${process.env.PORT}`);
})