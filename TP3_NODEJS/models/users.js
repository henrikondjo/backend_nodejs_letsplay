const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        require: [true, 'email is require'],
    },
    password: {
        type: String,
        require: [true, 'email is require'],
    },
    firstname: {
        type: String,
        require: [true, 'password is require'],
    },
    lastname: {
        type: String,
        require: [true, 'password is require'],
    },
    wins: {
        type: Number,
        default: 0,
    },
    losts: {
        type: Number,
        default: 0
    }


});
module.exports = mongoose.model('users', UserSchema);