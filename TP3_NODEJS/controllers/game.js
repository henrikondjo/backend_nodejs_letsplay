const users = require('../models/users.js');
// ROUTE POST/game
// DESC add points to one users
// ACCESS Private
exports.postwin = async (req, res) => {
    const { userid } = req;
    //recherche de l'utilisateur avec ce id
    const filter = { '_id': userid }
    const utilisateur = await users.findOne(filter);
    //on accede a la valeur wins de l'utilisateur puis on ajoute 1
    let gain = utilisateur.wins + 1;
    await users.updateOne(filter, { 'wins': gain })
    res.send({ 'success': true })
}
// ROUTE POST/game
// DESC add points to one users
// ACCESS Private
exports.postlost = async (req, res) => {
    const { userid } = req;
    //recherche de l'utilisateur avec ce id
    const filter = { '_id': userid }
    const utilisateur = await users.findOne(filter);
    let lost = utilisateur.losts + 1;//on accede a la valeur losts de l'utilisateur puis on ajoute 1
    await users.updateOne(filter, { 'losts': lost }) //nouvelle valeur modifier de losts
    res.send({ 'success': true })
}