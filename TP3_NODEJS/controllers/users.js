const users = require('../models/users.js');
//si le firstname n'est pas valide 
const Isfirstnamenotvalid = (firstname) => {
    if (firstname == "") return true
    if (firstname === undefined) return true
    return false
}
//si le lastname n'est pas valide 
const Islastnamenotvalid = (lastname) => {
    if (lastname == "") return true
    if (lastname === undefined) return true
    return false
}
//ROUTE GET/users
//DESC get all users
//ACCESS Private
exports.getall = async (req, res) => {
    const all = await users.find();
    res.send({ 'success': true, 'data': all });
}
//ROUTE GET/users/me
//DESC get one user
//ACCESS Private
exports.getme = async (req, res) => {

    const { userid } = req;
    const filter = { '_id': userid }
    //afficher les données de l'utilisateur avec un token 
    const me = await users.findOne(filter);
    res.send({ 'success': true, 'data': me })

}
//ROUTE PUT/users/me
//DESC update one user
//ACCESS Private
exports.putme = async (req, res) => {
    const { lastname, firstname } = req.body;
    //validation des informations
    if (Isfirstnamenotvalid(firstname)) return res.send({ 'success': false, 'Invalid': 'firstname' });
    if (Islastnamenotvalid(lastname)) return res.send({ 'success': false, 'Invalid ': 'lastname' });
    const { userid } = req;
    const filter = { '_id': userid }
    //modifier les informations du user connecté
    await users.updateOne(filter, { 'lastname': lastname, 'firstname': firstname })
    res.send({ 'success': true })
}
//ROUTE DELETE/users/me
//DESC delete one user
//ACCESS Private
exports.deleteme = async (req, res) => {
    const { userid } = req;
    const filter = { '_id': userid }
    //supprimer le user avec le id valide 
    await users.deleteOne(filter);
    res.send({ 'success': true })
}
//ROUTE PUT/users/me/reset-me
//DESC update game one user
//ACCESS Private
exports.resetme = async (req, res) => {
    const { userid } = req;             //recuperation de notre id dans notre token venant du midleware
    const filter = { '_id': userid }    //recherche de l'utilisateur avec ce id
    await users.updateOne(filter, { wins: 0, losts: 0 })
    res.send({ 'success': true })
}
