const users = require('../models/users.js');
const bcrypt = require('bcryptjs');
const jwtoken = require('jsonwebtoken');
const nodemailer = require('nodemailer');


//si le email n'est pas valide 
const Isemailnotvalid = (email) => {
    if (email == "") return true
    if (email === undefined) return true
    return false
}
//si le firstname n'est pas valide 
const Isfirstnamenotvalid = (firstname) => {
    if (firstname == "") return true
    if (firstname === undefined) return true
    return false
}
//si le lastname n'est pas valide 
const Islastnamenotvalid = (lastname) => {
    if (lastname == "") return true
    if (lastname === undefined) return true
    return false
}
//si le password n'est pas valide 
const Ispasswordnotvalid = (psw) => {
    if (psw == "") return true
    if (psw === undefined) return true
    return false
}
//ROUTE POST/auth/login
//DESC POST one user
//ACCESS Public

exports.login = async (req, res) => {
    const { email, password } = req.body;
    //validation des informations 
    if (Isemailnotvalid(email)) return res.send('Invalid information');
    if (Ispasswordnotvalid(password)) return res.send('Invalid information');
    const filter = { 'email': email };
    const user = await users.findOne(filter);
    if (user === null) {
        return res.send({ 'Success ': false, 'msg': `Invalid email information` });
    }
    const ismatch = await bcrypt.compare(password, user.password);
    if (!ismatch) {
        return res.send({ 'Success ': false, 'msg': `Invalid password information` });
    }
    //creation du token du user connecté
    const token = jwtoken.sign({ id: user.id }, process.env.JWT_SECRETKEY, { expiresIn: process.env.JWT_EXPIRE })
    res.send({ 'success': true, 'token': token });


}
const usertemp = [{}];

exports.register = async (req, res) => {
    const { firstname, lastname, email, password } = req.body;
    for (let i = 0; i < usertemp.length; i++) {
        if (usertemp[i].email === email) {
            return res.send({ 'success': false, 'msg': `il existe deja un user avec ce courriel ${email}` });
        }
    }
    if (Isemailnotvalid(email)) return res.send({ 'success': true, 'Invalid': 'email' });
    if (Ispasswordnotvalid(password)) return res.send({ 'success': true, 'Invalid': 'password' });
    if (Isfirstnamenotvalid(firstname)) return res.send({ 'success': true, 'Invalid': 'firstname' });
    if (Islastnamenotvalid(lastname)) return res.send({ 'success': true, 'Invalid': 'lastname' });
    let code = Math.floor(Math.random() * 900000) + 100000;
    const transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,     //"smtp-mail.gmail.com"
        port: process.env.SMTP_PORT,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASS
        },
    });
    await transporter.sendMail({
        from: 'votre genie favoris <HenriKondjo@teccart.online>',
        to: `${email}`, //le user 
        subject: 'Demande de confirmation',
        text: "Votre demande de confirmation",
        html: `<body>
        <h1>bonjour ${email}</h1>
        <div>
            <h6>Votre demande de confirmation est : ${code}</h6>
            
        </div>
    </body>`
    });

    const newusertemp =
    {
        'lastname': lastname,
        'firstname': firstname,
        'email': email,
        'password': password,
        'code': code
    }
    usertemp.push(newusertemp);
    // console.log(usertemp)
    res.send({ 'success': true });
}
exports.validateEmail = async (req, res) => {
    let usertrouv = "";
    const { email, code } = req.body;
    for (let i = 0; i < usertemp.length; i++) {
        if (usertemp[i].email === email && usertemp[i].code === code) {
            usertrouv = usertemp[i];//on stoke notre utilisateur trouvé dans une variable
        }
    }
    if (!usertrouv) {
        return res.send({ 'success': false, 'msg': 'error Invalid information' });
    }
    const filter = { 'email': usertrouv.email };     //on cree notre filter avec le email
    const userRech = await users.findOne(filter);  //on cherche s'il na pas un utilisateur avec notre email trouvé
    if (userRech) {
        return res.send({ 'success': false, 'msg': `il existe deja  un user avec ce courriel ${usertrouv.email}` });
    }
    const salt = await bcrypt.genSalt(10); //on cree notre salt le nombre de fois que l,on veut encrypté notre password
    const bcryptpassword = await bcrypt.hash(`${usertrouv.password}`, salt) //on encrypte notre password
    const addnew = await users.create({ email: usertrouv.email, password: bcryptpassword, firstname: usertrouv.firstname, lastname: usertrouv.lastname })
    //const token = jwtoken.sign(process.env.JWT_SECRETKEY, { expiresIn: process.env.JWT_EXPIRE })
    res.send({ 'success': true, 'token': 'true' });
}