const Router = require('express');
const { getall, getme, putme, deleteme, resetme } = require('../controllers/users');
const { protect } = require('../midleware/auth');
const router = Router({ mergeParams: false });//nous n'allons pas recevoir de donnée par l'url
router.route('/').get(protect, getall);
router.route('/me').get(protect, getme);
router.route('/me').put(protect, putme).delete(protect, deleteme);
router.route('/me/reset-stats').put(protect, resetme);


module.exports = router;