const Router = require('express');
const { postwin, postlost } = require('../controllers/game');
const { protect } = require('../midleware/auth');
const router = Router({ mergeParams: false }); //nous n'allons pas recevoir de donnée par l'url
router.route('/win').post(protect, postwin);
router.route('/lost').post(protect, postlost);
module.exports = router;