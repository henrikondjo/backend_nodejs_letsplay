const Router = require('express');
const { login, register, validateEmail } = require('../controllers/auth');
const router = Router({ mergeParams: false });//nous n'allons pas recevoir de donnée par l'url
router.route('/login').post(login);
router.route('/register').post(register);
router.route('/register/validate-email').post(validateEmail);
module.exports = router;